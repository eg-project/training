from bottle import route, run, template


@route('/hello/world')
    return template('<b> Hello world!')


if __name__ == '__main__':
    run(host='0.0.0.0', port=environ.get('PORT', 8080))